docs_sidekick:
	npx typedoc --out                sidekick_dist/docs \
	            --entryPointStrategy expand \
	            --name               Sidekick \
	            --readme             sidekick_dist/DOCS_INDEX.md \
	            sidekick_dist/sidekick/src_ts
