/********************************************************************
** Node API: functions for talking to an Aeternity node
**
** In the future, everything that this module does will be replaced
** by the backend.
**
** Functions should be sorted in alphabetical order.
**
** Names are what they are in the documentation
**
** Useful links:
**
**  - HTML API docs : https://api-docs.aeternity.io/
**  - YAML API docs : https://github.com/aeternity/aeternity/blob/master/apps/aehttp/priv/swagger.yaml
**
********************************************************************/
import * as net from './net.js';
import * as ae_compiler from './ae_compiler.js';
//-------------------------------------------------------------------
// CONSTANTS
//-------------------------------------------------------------------
export const MIN_FEE = 16660000000000;
export const MIN_CONTRACT_FEE = 79080000000000;
export const MIN_GAS_PRICE = 1000000000;
export const URL_MAINNET = "https://mainnet.aeternity.io/v2";
export const URL_TESTNET = "https://testnet.aeternity.io/v2";
async function GetAccountNextNonce(endpoint_url, params) {
    let pubkey = params.pubkey;
    let url = `${endpoint_url}/accounts/${pubkey}/next-nonce`;
    // if the "strategy" field is present, add it as a ?strategy=x
    // option
    //
    // note if the field is absent from `params`, then
    // `params.strategy` will be `undefined`, which in js whacko
    // world is "falsy"
    let strategy = params.strategy;
    if (strategy) {
        let addon = `?strategy=${strategy}`;
        url += addon;
    }
    // irrespective of the response code, this is what we return
    // so branching is gay
    let ret = await net.get_json(url);
    return ret;
}
async function PostContractCreate(endpoint_url, body_obj) {
    // console.log('body_obj', body_obj);
    let url = `${endpoint_url}/debug/contracts/create`;
    let ret = await net.post_json_response(url, body_obj);
    return ret;
}
async function create_contract(whoami, code, filename, init_args) {
    let code_resp = await ae_compiler.CompileContract(code, filename);
    let code_json = await code_resp.json();
    let bytecode = code_json.bytecode;
    let calldata_resp = await ae_compiler.EncodeCalldata(code, filename, "init", init_args);
    // assert(calldata_resp.ok);
    let calldata_json = await calldata_resp.json();
    let calldata = calldata_json.calldata;
    let cctx = { owner_id: whoami,
        code: bytecode,
        vm_version: 7,
        abi_version: 3,
        deposit: 0,
        amount: 0,
        gas: 25000,
        gas_price: 1 * MIN_GAS_PRICE,
        fee: 1 * MIN_CONTRACT_FEE,
        call_data: calldata };
    let ret = await PostContractCreate(URL_TESTNET, cctx);
    return ret;
}
async function PostSpend(endpoint_url, body_obj) {
    let url = `${endpoint_url}/debug/transactions/spend`;
    let ret = await net.post_json(url, body_obj);
    return ret;
}
//-------------------------------------------------------------------
// PostTransaction
//
// Docs: https://api-docs.aeternity.io/#/contract/PostTransaction
//
// > Post a new transaction
//-------------------------------------------------------------------
async function PostTransaction(endpoint_url, body_obj) {
    // console.log('body_obj', body_obj);
    let url = `${endpoint_url}/transactions`;
    let ret = await net.post_json_response(url, body_obj);
    return ret;
}
//-------------------------------------------------------------------
// GetTransactionInfoByHash
//
// Docs: https://api-docs.aeternity.io/#/contract/GetTransactionInfoByHash
//-------------------------------------------------------------------
async function GetTransactionInfoByHash(endpoint_url, hash) {
    // console.log('body_obj', body_obj);
    let url = `${endpoint_url}/transactions/${hash}/info`;
    let ret = await net.get_json_response(url);
    return ret;
}
export { GetAccountNextNonce, PostContractCreate, create_contract, PostSpend, PostTransaction };
//# sourceMappingURL=ae_node.js.map